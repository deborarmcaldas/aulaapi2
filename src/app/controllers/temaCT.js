const express = require('express')

const authMiddleware = require('../middleware/authMW')
const Temas = require('../models/TemaMD')
const Artigos = require('../models/ArtigoMD')

const router = express.Router()

router.get('/', async (req, res) => {
    try {
        const temas = await Temas.find()
        res.send({ temas })
    } catch (error) {
        res.ststus(400).send({ error: 'Nenhum tema foi encontrado' })
    }

})

router.get('/:temaId', async (req, res) => {
    const temaId = req.params.temaId
    try {
        const temaFind = await Temas.findById(temaId)
        res.send({ temaFind })
    } catch (error) {
        return res.status(400).send({ error: 'Tema não foi encontrado' })

    }
})


router.post('/', async (req, res) => {
    try {
        const { titulo_tem, descricao_tem, artigos } = req.body
        const tema = await Temas.create({ titulo_tem, descricao_tem, user: req.userId })

        if (artigos) {
            await Promise.all(artigos.map(async artigo => {
                const temaArtigo = new Artigos({ ...artigo, tema: tema._id })
                await temaArtigo.save()
                tema.artigos.push(temaArtigo)
            }))

            await tema.save()
        }
        return res.send({ tema })

    } catch (error) {
        // console.log(error)
        return res.status(400).send({ error: 'Erro ao criar tema' })
    }
})

router.put('/:temaId', async (req, res) => {
    const temaId = req.params.temaId
    const { titulo_tem, descricao_tem, artigos } = req.body
    try {
        const tema = await Temas.findById(temaId)

        if (!tema)
            return res.status(400).send({ error: 'Tema não encontrado' })
        await Temas.findByIdAndUpdate(temaId, {
            titulo_tem,
            descricao_tem
        }), ({ new: true })

        tema.artigos = []
        await Artigos.remove({ tema: tema._id })

        if (artigos) {
            await Promise.all(artigos.map(async artigo => {
                const temaArtigo = new Artigos({ ...artigo, tema: tema._id })
                await temaArtigo.save()
                tema.artigos.push(temaArtigo)
            }))

            await tema.save()
        }
        res.send(`${temaId} Atualizado com sucesso`)

    } catch (error) {
        console.log(error)
        return res.status(400).send({ error: 'Não foi possível atualizar o tema' })
    }
})

router.delete('/:temaId', async (req, res) => {
    const temaId = req.params.temaId
    try {
        if (! await Temas.findByIdAndRemove(temaId))
            return res.status(400).send({ error: 'Item já foi excluído' })
        res.send(`${temaId} Excluído com sucesso`)
    } catch (error) {
        return res.status(400).send({ error: 'Não foi possível excluir o tema' })
    }
})



module.exports = app => app.use('/temas', router)