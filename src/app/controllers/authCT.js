const express = require('express')
const User = require('../models/UserMD')
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')
const router = express.Router()
const authConfig = require('../../config/auth.json')
const authMDW = require('../middleware/authMW')
const crypto = require('crypto')
const mailer = require('../../modules/mailer')

function generateToken(params = {}) {
    return jwt.sign(
        params,
        authConfig.secret,
        {
            expiresIn: 86400
        }
    )
}

router.post('/authenticate', async function (req, res) {
    const { email, password } = req.body
    const user = await User.findOne({ email }).select('+password')

    if (!user)
        return res.status(400).send({ error: 'Usuário não existe' })

    if (!await bcrypt.compare(password, user.password))
        return res.status(400).send({ error: 'Password inválido' })

    user.password = undefined


    return res.send({ user, token: generateToken({ id: user.id }) })
})

// router.use(authMDW)
router.post('/register', async function (req, res) {

    const { email } = req.body

    try {
        if (await User.findOne({ email }))
            return res.status(400).send({ error: 'Usuário já existe' })

        const user = await User.create(req.body)

        user.password = undefined
        return res.send({ user, token: generateToken({ id: user.id }) })

    } catch (error) {
        console.log(error)
        return res.status(400).send({ error: 'Registro de usuário falhou' })
    }

})

router.post('/forgot_password', async (req, res) => {
    const { email } = req.body

    try {
        const user = await User.findOne({ email })

        if (!user)
            return res.status(400).send({ error: 'Usuário não existe' })

        const token = crypto.randomBytes(20).toString('hex')

        const now = new Date()
        now.setHours(now.getHours + 1)

        const userResult = await User.findByIdAndUpdate(user.id, {
            '$set': {
                passwordResetToken: token,
                passwordResetExpires: now,
            }
        })

        mailer.sendMail({
            to: email,
            from: 'deb@gmail.com',
            template: 'auth/forgot_password',
            context: { token }
        }, (err) => {
            if (err)
                // console.log(err)
                return res.status(400).send({ error: 'Não foi possível recuperar a senha' })
            return res.send({ ok: 'Email enviado' })
        })

        router.post('/reset_password', async (req, res) => {
            const { email, token, password } = req.body
            try {
                const user = await User.findOne({ email })
                    .select('+passwordResetToken passwordResetExpires')

                if (!user)
                    return res.status(400).send({ error: 'Uusário não encontrado' })

                if (token !== user.passwordResetToken)
                    return res.status(400).send({ error: 'Token inválido' })

                const now = new Date()

                if (now > user.passwordResetExpires)
                    return res.status(400).send({ error: 'Token expirou, gere um novo' })
                user.password = password
                await user.save
                res.send({ ok: 'Recuperado' })

            } catch (error) {
                res.status(400).send({ error: 'Não foi possível resetar a senha, tente novamente' })
            }
        })

    } catch (error) {
        return res.status(400).send({ error: 'Erro ao recuperar a senha, tente novamente' })
    }
})


module.exports = app => app.use('/auth', router)  