const mongoose = require('../../database/dbConn')

const ArtigoSchema = new mongoose.Schema({
    titulo_art: {
        type: String,
        required: true,
    },
    descricao_art: {
        type: String,
        required: true,
    },
    imagem_artigo: {
        type: String,
        required: false,
    },
    autor: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: false,
    },
    tema: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Tema',
        required: false,
    },
    status: {
        type: Boolean,
        required: true,
        default: false,
    },
    createAt: {
        type: Date,
        default: Date.now,
    },

})

const Artigo = mongoose.model('Artigo', ArtigoSchema)
module.exports = Artigo