const bcrypt = require('bcryptjs')
const mongoose = require('../../database/dbConn')

const UserSchema = new mongoose.Schema({
    nome: {
        type: String,
        required: true,
    },
    email: {
        type: String,
        unique: true,
        required: true,
        lowercase: true,
    },
    password: {
        type: String,
        required: true,
        selected: false,
    },
    passwordResetToken: {
        type: String,
        required: false,
        selected: false,
    },
    passwordResetExpires: {
        type: String,
        required: false,
        selected: false,
    },
    createAt : {
        type: Date,
        default: Date.now,
    }
})

UserSchema.pre('save', async function(next){ 
    const hash = await bcrypt.hash(this.password, 8)
    this.password = hash
    next()
})


const User = mongoose.model('User', UserSchema)
module.exports = User