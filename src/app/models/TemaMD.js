const mongoose = require('../../database/dbConn')

const TemaSchema = new mongoose.Schema({
    titulo_tem: {
        type: String,
        required: true,
        unique: true,
    },
    descricao_tem: {
        type: String,
        required: false,
    },
    user:{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User',
        required: false,
    },
    artigos:[{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Artigo',
    }],
    createAt:{
        type: Date,
        default: Date.now,
    },

})

const Tema = mongoose.model('Tema', TemaSchema)
module.exports = Tema