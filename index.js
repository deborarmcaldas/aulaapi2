
const express = require('express')
const bodyParser = require('body-parser')
const app = express ()

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({extended: false}))

app.listen(3000, console.log('servidor na porta', 3000))

require('./src/app/controllers/indexCT')(app)
// require('./src/app/controllers/authCT')(app)

module.exports = app